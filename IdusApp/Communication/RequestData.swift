//
//  RequestData.swift
//  IdusApp
//
//  Created by 김우성 on 2020/07/16.
//  Copyright © 2020 김우성. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class RequestData: NSObject {
    // 기본 url
    static let BaseUrlPath = "https://itunes.apple.com"
    
    // Sub url
    enum SubUrl {
      static let SearchPath = "/search?"
    }
    
    // 검색 API
    func getSearchAPI(searchKey: String, completion:  @escaping (StoreModel?) -> Void) -> Void{
        
        // 파라미터 추가
        // 일단 API가 한개라서 나머지 값은 고정으로 적용
        let stringURL = RequestData.BaseUrlPath +  SubUrl.SearchPath+"term=\(searchKey)&"+"country=kr&"+"media=software"
        guard let url: URL = Common.urlEncoding(strUrl: stringURL) else {
            return completion(nil)
        }
            
        getRequestAPI(url: url, completion: completion)
    }
    
    // Get 방식 요청
    fileprivate func getRequestAPI(url: URL, completion:  @escaping (StoreModel?) -> Void) -> Void {
        Alamofire.request(url).responseJSON(completionHandler: { response in
            
            switch response.result {
                case .success(let value):
                    let jsonValue = JSON(value)
                    let data:StoreModel? = StoreModel.init(json: jsonValue)
                    completion(data)
                case .failure(_):
                    completion(nil)
                }
        })
    }
    
    // Post 방식 요청
    fileprivate func postRequestAPI(url: URL, completion:  @escaping (StoreModel?) -> Void) -> Void {
        Alamofire.request(url, method: .post).responseJSON(completionHandler: { response in
            
            switch response.result {
                case .success(let value):
                    let jsonValue = JSON(value)
                    print(jsonValue)
                case .failure(_):
                    completion(nil)
                }
        })
    }
}
