//
//  ViewController.swift
//  IdusApp
//
//  Created by 김우성 on 2020/07/16.
//  Copyright © 2020 김우성. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let HeaderHeight:CGFloat = 50.0
    var searchKey = "핸드메이드"
    var storeData:StoreModel? = nil
    @IBOutlet weak var storeTableView:UITableView? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let backItem = UIBarButtonItem()
        backItem.title = searchKey
        navigationItem.backBarButtonItem = backItem

        // call request
        RequestData().getSearchAPI(searchKey: searchKey, completion: {
            [weak self] data in
            if data != nil {
                self?.storeData = data
                self?.storeTableView?.reloadData()
            } else {
                print("request error")
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true

    }
}


// main view table delegate extention
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    // header height
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return HeaderHeight
    }
    
    // headerView
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: Int(tableView.frame.size.width), height: Int(HeaderHeight)))
        headerView.backgroundColor = UIColor.white
        let titlelabel = UILabel(frame: CGRect(x: 10, y: 0, width: headerView.bounds.width-10, height: headerView.bounds.height))
        titlelabel.font = UIFont.boldSystemFont(ofSize: 30)
        titlelabel.text = searchKey
        headerView .addSubview(titlelabel)
        
        // add border line
        let bottomBorder:CALayer = CALayer()
        bottomBorder.frame = CGRect.init(x: 0, y: headerView.bounds.height - 1, width: headerView.bounds.width, height: 1)
        bottomBorder.backgroundColor = UIColor.gray.cgColor
        return headerView
    }
    
    // cell row count
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.storeData?.count ?? 0
    }
    
    // cell create
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "StoreMainCell", for: indexPath) as! StoreMainCell
        guard let detailData = self.storeData?.storeArray[indexPath.row] else {
            print("data not found")
            return cell
        }

        cell.setData(storeDetail: detailData)

        return cell
    }
    
    // cell select
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let detailData = self.storeData?.storeArray[indexPath.row] else {
            print("data not found")
            return
        }
        let detailViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "StoreDetailViewController") as! StoreDetailViewController
        detailViewController.setDetailData(detailData: detailData)
        self.navigationController?.pushViewController(detailViewController, animated: false)
    }
}
