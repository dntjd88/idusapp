//
//  StoreDetailViewController.swift
//  IdusApp
//
//  Created by 김우성 on 2020/07/16.
//  Copyright © 2020 김우성. All rights reserved.
//

import UIKit

fileprivate class CellData {
    var tableStyle:String = ""
    var text:String = ""
    var subText:String = ""
    var subViewFlag:Bool = false
    var subViewText:String = ""

    init?(style:String!, text: String = "", subText:String = "", subViewText:String = "") {
        self.tableStyle = style
        self.text = text
        self.subText = subText
        self.subViewText = subViewText
    }
}

class StoreDetailViewController: UIViewController {
    var storeDetail:StoreDetail? = nil
    fileprivate var listData: Array<CellData> = []
    @IBOutlet weak var detailTableView:UITableView? = nil

    fileprivate enum TableStyle {
        static let Top = "StoreDetailTopCell"           // 상단 이미지, 가격, 버튼 표시 Cell
        static let Common = "StoreDetailCell"           // 양쪽 텍스트 표시 Cell
        static let Sub = "StoreDetailSubCell"           // Common Cell 선택시 표시하는 Sub Cell
        static let Text = "StoreDetailTextCell"         // Text 표시 Cell
        static let Category = "StoreDetailCategoryCell" // 카테고리 표시 Cell
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }

    // 데이터 셋팅
    func setDetailData(detailData: StoreDetail) {
        self.listData.removeAll()
        self.storeDetail = detailData
        
        guard let getDetailData = self.storeDetail else {
            return
        }
        
        // 상단 이미지, 가격, 버튼 표시 Cell
        if let topCell = CellData.init(style: TableStyle.Top) {
            listData.append(topCell)
        }
        
        // 양쪽 텍스트 표시 Cell
        let megaByte = CGFloat(NSString(string: getDetailData.fileSizeBytes).floatValue) / 1024 / 1024
        let strMegaByte = String(format: "%.1f MB", megaByte)
        if let ComCell = CellData.init(style: TableStyle.Common, text: "크기", subText: strMegaByte) {
            listData.append(ComCell)
        }
        if let ComCell = CellData.init(style: TableStyle.Common, text: "연령", subText: getDetailData.trackContentRating) {
            listData.append(ComCell)
        }
        if let ComCell = CellData.init(style: TableStyle.Common, text: "새로운 기능", subText: getDetailData.version, subViewText: getDetailData.releaseNotes){
            listData.append(ComCell)
        }
        
        // 설명 Cell
        if let TextCell = CellData.init(style: TableStyle.Text, text: getDetailData.description) {
            listData.append(TextCell)
        }
        
        // 카테고리 Cell
        if let CategoryCell = CellData.init(style: TableStyle.Category, text: "카테고리") {
            listData.append(CategoryCell)
        }

        self.detailTableView?.reloadData()
    }
    
    
    //MARK: - Button Event
    // 웹에서 보기
    @objc func WebLinkButtnClick (sender : UIButton) {
        guard let getDetailData = self.storeDetail else {
            return
        }
        
        let alertViewController = UIAlertController(title: "알림", message: "웹 사이트로 이동하시겠습니까?", preferredStyle: .alert)

        let okButton = UIAlertAction(title: "확인", style: .default, handler: { (action) in
            Common.goToUrl(strUrl: getDetailData.trackViewUrl)
        })
        let cancelButton = UIAlertAction(title: "취소", style: .cancel, handler: nil)
        alertViewController.addAction(okButton)
        alertViewController.addAction(cancelButton)

        self.present(alertViewController, animated: true, completion: nil)
    }
    
    // 공유하기
    @objc func shareButtnClick (sender : UIButton) {
        guard let getDetailData = self.storeDetail else {
            return
        }
        
        guard let trackViewUrl = URL(string: getDetailData.trackViewUrl) else {
            return
        }
        
        let shareData = [ trackViewUrl ] as [Any]
        let activityViewController = UIActivityViewController(activityItems: shareData, applicationActivities: nil)
        
        activityViewController.popoverPresentationController?.sourceView = self.view // 안넣으면 iPad 크래쉬 난다고함
        // 제외하고 싶은 타입을 설정
        activityViewController.excludedActivityTypes = [
            UIActivity.ActivityType.saveToCameraRoll, // 사진첩
            UIActivity.ActivityType.assignToContact,   // 전화번호부
        ]

        DispatchQueue.main.async {
            self.present(activityViewController, animated: true, completion: nil)
        }
        //Completion handler
        activityViewController.completionWithItemsHandler = { (activityType: UIActivity.ActivityType?, completed:
        Bool, arrayReturnedItems: [Any]?, error: Error?) in
            if completed {
                print("share completed")
                return
            } else {
                print("cancel")
            }
            if let shareError = error {
                print("error while sharing: \(shareError.localizedDescription)")
            }
        }
    }
}


// main view table delegate extention
extension StoreDetailViewController: UITableViewDelegate, UITableViewDataSource {
    // cell row count
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listData.count
    }
    
    // cell create
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellData:CellData = listData[indexPath.row]
        
        if cellData.tableStyle == TableStyle.Top {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableStyle.Top, for: indexPath) as! StoreDetailTopCell
            
            guard let getDetailData = self.storeDetail else {
                return cell
            }

            cell.setData(storeDetail: getDetailData)
            cell.leftWebButton.addTarget(self, action: #selector(StoreDetailViewController.WebLinkButtnClick(sender:)), for: .touchUpInside)
            cell.rightShareButton.addTarget(self, action: #selector(StoreDetailViewController.shareButtnClick(sender:)), for: .touchUpInside)
            
            return cell
        } else if cellData.tableStyle == TableStyle.Common {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableStyle.Common, for: indexPath) as! StoreDetailCell
            if cellData.subViewText.count > 0 {
                cell.setData(text: cellData.text, subText: cellData.subText, subViewFlag: cellData.subViewFlag)
            } else {
                cell.setData(text: cellData.text, subText: cellData.subText)
            }

            return cell
        } else if cellData.tableStyle == TableStyle.Sub {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableStyle.Sub, for: indexPath) as! StoreDetailSubCell
            cell.setData(text: cellData.text)

            return cell
        } else if cellData.tableStyle == TableStyle.Text {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableStyle.Text, for: indexPath) as! StoreDetailTextCell
               
            cell.setData(text: cellData.text)
            
            return cell
        } else if cellData.tableStyle == TableStyle.Category {
            let cell = tableView.dequeueReusableCell(withIdentifier: TableStyle.Category, for: indexPath) as! StoreDetailCategoryCell
            guard let getDetailData = self.storeDetail else {
                return cell
            }

            cell.setData(text: cellData.text, genres: getDetailData.genres)

            return cell
        }

        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // SubView 데이터 있으면 표시 / 비표시

        let cellData:CellData = listData[indexPath.row]
        if cellData.tableStyle == TableStyle.Common && cellData.subViewText != "" {
            self.detailTableView?.beginUpdates()

            let index:IndexPath = IndexPath(row: indexPath.row + 1, section: 0)
            if cellData.subViewFlag {
                listData.remove(at: index.row)
                cellData.subViewFlag = false
                self.detailTableView?.deleteRows(at: [index], with: .bottom)

            } else {
                if let SubCell = CellData.init(style: TableStyle.Sub, text: cellData.subViewText){
                    listData.insert(SubCell, at: index.row)
                    cellData.subViewFlag = true
                    self.detailTableView?.insertRows(at: [index], with: .bottom)
                }
            }
            tableView.reloadRows(at: [indexPath], with: .automatic)
            listData[indexPath.row] = cellData
            
            self.detailTableView?.endUpdates()
        }
        
    }
}
