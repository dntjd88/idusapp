//
//  StoreDetailTopCell.swift
//  IdusApp
//
//  Created by 김우성 on 2020/07/17.
//  Copyright © 2020 김우성. All rights reserved.
//

import UIKit
import Alamofire

class StoreDetailTopCell: UITableViewCell {
    @IBOutlet weak var scrollView:UIScrollView!
    @IBOutlet weak var stackView:UIStackView!

    @IBOutlet weak var artistNameLabel: UILabel!        // 만든이
    @IBOutlet weak var trackCensoredNameLabel: UILabel! // 앱이름
    @IBOutlet weak var priceLabel: UILabel!                  // 가격

    @IBOutlet weak var leftWebButton:UIButton!             // 왼쪽 버튼
    @IBOutlet weak var rightShareButton:UIButton!            // 오른쪽 버튼
    @IBOutlet weak var buttonView:UIView!             // 왼쪽 버튼

    @IBOutlet var stackViewWidthConstraint:NSLayoutConstraint!

    let stackViewWidth:CGFloat = 250.0
    var arrayView: Array<UIImageView> = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none

        self.leftWebButton.setTitle("웹에서 보기", for: .normal)
        self.leftWebButton.backgroundColor = UIColor.white
        self.rightShareButton.setTitle("공유 하기", for: .normal)
        self.rightShareButton.backgroundColor = UIColor.white
        
        self.buttonView.backgroundColor = UIColor.black
        self.buttonView.layer.masksToBounds = true
        self.buttonView.layer.cornerRadius = 8.0
        self.buttonView.layer.borderWidth = 1.0
        self.buttonView.layer.borderColor = UIColor.black.cgColor

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setClearCell(){
        
    }

    func setData(storeDetail: StoreDetail) {
        self.artistNameLabel.text = storeDetail.artistName
        self.trackCensoredNameLabel.text = storeDetail.trackCensoredName
        if storeDetail.formattedPrice == "무료" {
            self.priceLabel.text = storeDetail.formattedPrice
        } else {
            let attributesPrice = [NSAttributedString.Key.font: UIFont.boldSystemFont(ofSize: 30)]
            let attPriceString = NSMutableAttributedString(string: storeDetail.price, attributes: attributesPrice)
            
            let attributesUnit = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20)]
            let attUnitString = NSAttributedString(string: " 원", attributes: attributesUnit)
            attPriceString.append(attUnitString)
            self.priceLabel.attributedText = attPriceString
        }
        self.addScreenShotView(screenshotUrls: storeDetail.screenshotUrls)
    }

    // 이미지 뷰 추가
    func addScreenShotView(screenshotUrls : Array<String>) {
        if self.arrayView.count > 0 {
            return
        }
        var toView:UIImageView? = nil
        for screenshotUrl in screenshotUrls {
            if self.arrayView.count > 0 {
                toView = self.arrayView.last
            }
            
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: stackViewWidth, height: self.scrollView.frame.height))
            imageView.layer.masksToBounds = true
            imageView.layer.cornerRadius = 10.0
            self.arrayView.append(imageView)
            self.stackView.addArrangedSubview(imageView)
            
            if toView != nil {
                NSLayoutConstraint(item: imageView,
                                   attribute: .width,
                                   relatedBy: .equal,
                                   toItem: toView,
                                   attribute: .width,
                                   multiplier: 1,
                                   constant: 0).isActive = true
            }
            
            Alamofire.request(screenshotUrl, method: .get).response { response in
                guard let image = UIImage(data:response.data!) else {
                    return
                }
                let imageData = image.jpegData(compressionQuality: 1.0)
                imageView.image = UIImage(data : imageData!)
            }
            
        }
        self.stackViewWidthConstraint.constant = stackViewWidth * CGFloat(self.arrayView.count)
    }
    
    // 그리기전 화면 삭제
    func removeScrenShotView() {
        for view in self.arrayView {
            view.removeFromSuperview()
        }
        self.arrayView.removeAll()
    }
}
