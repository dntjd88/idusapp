//
//  StoreDetailCategoryCell.swift
//  IdusApp
//
//  Created by 김우성 on 2020/07/17.
//  Copyright © 2020 김우성. All rights reserved.
//

import UIKit

class StoreDetailCategoryCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var genresView: UIView!
    var arrayView: Array<UIButton> = []

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func setData(text: String, genres: Array<String> = []) {
        self.titleLabel.text = text
        if self.arrayView.count == 0 {
            for genre in genres {
                let genreButton = UIButton(frame: CGRect(x: 0, y: 0, width: 0, height: self.genresView.frame.height))
                genreButton.layer.masksToBounds = true
                genreButton.layer.cornerRadius = 8.0
                genreButton.layer.borderWidth = 1.0
                genreButton.layer.borderColor = UIColor.black.cgColor

                genreButton.setTitle(genre, for: .normal)
                genreButton.setTitleColor(.black, for: .normal)
                genreButton.isUserInteractionEnabled = false
                genreButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
                
                if self.arrayView.count == 0 {
                    self.genresView.addSubview(genreButton)
                    genreButton.translatesAutoresizingMaskIntoConstraints = false
                    genreButton.topAnchor.constraint(equalTo: self.genresView.topAnchor, constant: 0).isActive = true
                    genreButton.leadingAnchor.constraint(equalTo: self.genresView.leadingAnchor, constant: 0).isActive = true
                    genreButton.bottomAnchor.constraint(equalTo: self.genresView.bottomAnchor, constant: 0).isActive = true
                } else {
                    guard let setToView = self.arrayView.last else {
                        return
                    }
                    if self.genresView.frame.width < setToView.frame.size.width + setToView.frame.origin.x {
                        return
                    } else {
                        self.genresView.addSubview(genreButton)
                        genreButton.translatesAutoresizingMaskIntoConstraints = false
                        genreButton.topAnchor.constraint(equalTo: self.genresView.topAnchor, constant: 0).isActive = true
                        genreButton.leadingAnchor.constraint(equalTo: setToView.trailingAnchor, constant: 5).isActive = true
                        genreButton.bottomAnchor.constraint(equalTo: self.genresView.bottomAnchor, constant: 0).isActive = true
                    }
                }
                self.arrayView.append(genreButton)
            }
        }
    }
}
