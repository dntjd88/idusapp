//
//  StoreDetailCell.swift
//  IdusApp
//
//  Created by 김우성 on 2020/07/17.
//  Copyright © 2020 김우성. All rights reserved.
//

import UIKit

class StoreDetailCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subLabel: UILabel!
    @IBOutlet weak var arrowImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(text: String, subText: String) {
        self.titleLabel.text = text
        self.subLabel.text = subText
        self.arrowImage.image = nil
        self.arrowImage.isHidden = true
    }
    
    
    func setData(text: String, subText: String, subViewFlag: Bool = false) {
        self.titleLabel.text = text
        self.subLabel.text = subText
        self.arrowImage.isHidden = false
        
        // 서브 화살표 이미지 표시
        if subViewFlag {
            self.arrowImage.image = UIImage(named: "up")
        } else {
            self.arrowImage.image = UIImage(named: "down")
        }
    }


}
