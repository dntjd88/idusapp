//
//  StoreMainCell.swift
//  IdusApp
//
//  Created by 김우성 on 2020/07/16.
//  Copyright © 2020 김우성. All rights reserved.
//

import UIKit
import Alamofire

class StoreMainCell: UITableViewCell {
    @IBOutlet weak var roundView:UIView!                                // 테두리 뷰
    @IBOutlet weak var artworkUrl512Image:UIImageView!                  // 앱이미지
    @IBOutlet weak var artistNameLabel: UILabel!                        // 만든이
    @IBOutlet weak var trackCensoredNameLabel: UILabel!                 // 앱이름
    @IBOutlet weak var genresLable:UILabel!                             // 카테고리
    @IBOutlet weak var formattedPriceLabel:UILabel!                     // 판매상태
    @IBOutlet weak var averageUserRatingForCurrentVersionView: UIView!  // 별점 뷰
    @IBOutlet weak var startLayoutConstraint: NSLayoutConstraint!

//    averageUserRatingForCurrentVersion
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none

        self.contentView.backgroundColor = UIColor.gray
        self.roundView.backgroundColor = UIColor.gray
        self.roundView.layer.masksToBounds = true
        self.roundView.layer.cornerRadius = 10.0
        self.roundView.layer.borderWidth = 1.0
        self.roundView.layer.borderColor = UIColor.black.cgColor
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(storeDetail: StoreDetail) {
        
        self.artistNameLabel.text = storeDetail.artistName
        self.trackCensoredNameLabel.text = storeDetail.trackCensoredName
        self.formattedPriceLabel.text = storeDetail.formattedPrice

        // 별점 있으면 표시 / 비표시
        let userRating = CGFloat(NSString(string: storeDetail.averageUserRatingForCurrentVersion).floatValue)
        if userRating == 0 {
            self.averageUserRatingForCurrentVersionView.isHidden = true
        } else {
            self.averageUserRatingForCurrentVersionView.isHidden = false
            self.startLayoutConstraint.constant = (self.averageUserRatingForCurrentVersionView.frame.size.width / 5) * userRating
        }
        
        Alamofire.request(storeDetail.artworkUrl512, method: .get).response { response in
            guard let image = UIImage(data:response.data!) else {
               self.artworkUrl512Image.image = UIImage(named: "defalut")
               return
            }
            let imageData = image.jpegData(compressionQuality: 1.0)
            self.artworkUrl512Image.image = UIImage(data : imageData!)
        }
        guard let genre = storeDetail.genres.first else {
            self.genresLable.text = ""
            return
        }
        self.genresLable.text = genre
    }
}
