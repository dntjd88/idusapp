//
//  StoreDetailTextCell.swift
//  IdusApp
//
//  Created by 김우성 on 2020/07/17.
//  Copyright © 2020 김우성. All rights reserved.
//

import UIKit

class StoreDetailTextCell: UITableViewCell {
    @IBOutlet weak var descriptionLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(text: String) {
        self.descriptionLabel.text = text
    }
}
