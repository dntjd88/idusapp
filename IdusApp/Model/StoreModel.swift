//
//  StoreModel.swift
//  IdusApp
//
//  Created by 김우성 on 2020/07/16.
//  Copyright © 2020 김우성. All rights reserved.
//

import UIKit
import SwiftyJSON

class StoreModel {
    // Store Count
    var count = 0
    
    // Store Data
    var storeArray: Array<StoreDetail> = []
    init?(json:JSON) {
        for jsonData in json["results"].arrayValue {
            guard let storeDetail:StoreDetail = StoreDetail.init(json: jsonData) else {
                continue
            }
            self.storeArray.append(storeDetail)
        }
        self.count = self.storeArray.count
    }
}

// 상세 데이터
class StoreDetail {
    var artistName: String = ""                             // 만든이
    var trackName: String = ""                              // 앱 이름
    var trackCensoredName: String = ""                      // 타이틀 이름
    var currentVersionReleaseDate: String = ""              // 현재 버전 릴리즈 날짜
    var averageUserRatingForCurrentVersion: String = ""     // 별점
    var fileSizeBytes: String = ""                          // 파일 사이즈
    var description: String = ""                            // 설명
    var releaseDate: String = ""                            // 처음 릴리즈 날짜
    var artworkUrl512: String = ""                          // 앱이미지
    var screenshotUrls: Array<String> = []                  // 스크린샷
    var trackContentRating: String = ""                     // 연령
    var formattedPrice: String = ""                         // 판매상태
    var price: String = ""                                  // 가격
    var genres: Array<String> = []                          // 카테고리
    var sellerName: String = ""                             // 판매자 이름
    var trackViewUrl: String = ""                           // 웹 페이지 이동 Url
    var version: String = ""                                // 앱버전
    var releaseNotes: String = ""                           // 릴리즈 노트
    
    init?(json:JSON) {
        self.artistName = json["artistName"].stringValue
        self.trackName = json["trackName"].stringValue
        self.trackCensoredName = json["trackCensoredName"].stringValue
        self.currentVersionReleaseDate = json["currentVersionReleaseDate"].stringValue
        self.averageUserRatingForCurrentVersion = json["averageUserRatingForCurrentVersion"].stringValue
        self.fileSizeBytes = json["fileSizeBytes"].stringValue
        self.description = json["description"].stringValue
        self.releaseDate = json["releaseDate"].stringValue
        self.artworkUrl512 = json["artworkUrl512"].stringValue
        self.trackContentRating = json["trackContentRating"].stringValue
        self.formattedPrice = json["formattedPrice"].stringValue
        self.price = json["price"].stringValue
        self.sellerName = json["sellerName"].stringValue
        self.trackViewUrl = json["trackViewUrl"].stringValue
        self.version = json["version"].stringValue
        self.releaseNotes = json["releaseNotes"].stringValue

        for item in json["screenshotUrls"].arrayValue {
            self.screenshotUrls.append(item.stringValue)
        }
        
        for item in json["genres"].arrayValue {
            self.genres.append(item.stringValue)
        }
    }
}
