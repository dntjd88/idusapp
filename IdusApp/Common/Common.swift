//
//  Common.swift
//  IdusApp
//
//  Created by 김우성 on 2020/07/16.
//  Copyright © 2020 김우성. All rights reserved.
//

import UIKit

class Common: NSObject {

    // url 인코딩
    static func urlEncoding(strUrl:String) -> URL? {
        if let encoded  = strUrl.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed) {
            guard let encodingURL = URL(string: encoded) else {
                return nil
            }
            return encodingURL
        }
        return nil
    }
    
    // url 이동
    static func goToUrl(strUrl:String) {
        guard let url = Common.urlEncoding(strUrl: strUrl) else {
            return
        }
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        } else {
            print("error")
        }
    }
}
